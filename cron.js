const send = require("./sendMessage");
var admin = require('firebase-admin');
const http = require('http')
const https = require('https')
const fs = require('fs')
const rp = require("request-promise");
const request = require("request")
const bodyParser = require('body-parser');
var serviceAccount = require("./a7capitalticker-firebase-adminsdk-4vfhh-0a5ead45a0.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://a7capitalticker.firebaseio.com/'
});

class Crypto {
	constructor(asset, name) {
		this.asset = asset;
		this.name = name;
	}
}

var db = admin.database();
var ref = db.ref("crypto");
var obj = null;

getAssets().then(function(result) {
	return loadAssets(result);
}).then(function(secondresult) {
	return getCurrentPrice(secondresult);
}).then(function(finalresult) {

}).then(function() {
	return;
});

function loadAssets(result) {
	let assets = [];
	return new Promise((resolve, reject)=> {
		Object.keys(result).forEach(function(k){
			var obj = new Crypto(result[k], k);
			assets.push(obj);
			resolve(assets)
		});
	});
}

function getAssets() {
	return new Promise((resolve, reject) => {
		ref.on("value", function(snapshot) {
			obj = snapshot.val();
			resolve(obj);
		}, function (errorObject) {
			console.log("The read failed: " + errorObject.code);
		});
	})
}

function compareAsset(assets) {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	if(dd<10)  {
		dd='0'+dd;
	} 
	if(mm<10) {
		mm='0'+mm;
	}
	var i;
	var d = new Date(),
    h = (d.getHours()<10?'0':'') + d.getHours(),
    m = (d.getMinutes()<10?'0':'') + d.getMinutes();
  	i = h + ':' + m;
  	console.log(i);
	today = dd+'/'+mm
	console.log(today);
	assets.forEach(function(item, index, array) {
		if(item.cp>item.asset.price.target2) {
			if(item.asset.notification.on != "target2") {
				ref.child(item.name).child("notification").update({
					on: "target2"
				}); 
				//send notif
				callSendAPI(2639334339471672, item.asset.fullName + " a atteint la cible target 2 " + "Le " + today +" à " + i);
				callSendAPI(2615959411751110, item.asset.fullName + " a atteint la cible target 2 " + "Le " + today +" à " + i);
				callSendAPI(1963316460437389, item.asset.fullName + " a atteint la cible target 2 " + "Le " + today +" à " + i);
			}
		}

		if(item.cp < item.asset.price.target2 && item.cp > item.asset.price.target1) {
			if(item.asset.notification.on != "target1") {
				ref.child(item.name).child("notification").update({
					on: "target1"
				});
				callSendAPI(2639334339471672, item.asset.fullName + " a atteint la cible target 1" + "Le " + today +" à " + i);
				callSendAPI(2615959411751110, item.asset.fullName + " a atteint la cible target 1" + "Le " + today +" à " + i); 
				callSendAPI(1963316460437389, item.asset.fullName + " a atteint la cible target 1" + "Le " + today +" à " + i); 
			}
		}

		if(item.cp < item.asset.price.target2 && item.cp > item.asset.price.support) {
			if(item.asset.notification.on != "support") {
				ref.child(item.name).child("notification").update({
					on: "support"
				});
				
				callSendAPI(2639334339471672, item.asset.fullName + " a atteint la cible support Le " + today +" à " + i);
				callSendAPI(2615959411751110, item.asset.fullName + " a atteint la cible support Le " + today +" à " + i); 
				callSendAPI(1963316460437389, item.asset.fullName + " a atteint la cible support Le " + today +" à " + i); 
			}
		}
		if(item.cp < item.asset.price.stopLoss) {
			if(item.asset.notification.on != "stoploss") {
				ref.child(item.name).child("notification").update({
					on: "stoploss"
				});
				callSendAPI(2639334339471672, item.asset.fullName + " a atteint la cible stoploss Le " + today +" à " + i);
				callSendAPI(2615959411751110, item.asset.fullName + " a atteint la cible stoploss Le " + today +" à " + i);
				callSendAPI(1963316460437389, item.asset.fullName + " a atteint la cible stoploss Le " + today +" à " + i);  
			}
		}
		/*if(item.cp > item.asset.price.customPrice) {
			if(item.asset.notification.on != "customPrice") {
				ref.child(item.name).child("notification").update({
					on: "customPrice"
				});
				callSendAPI(2639334339471672, item.asset.fullName + " a atteint la cible customPrice Le " + today +" à " + i);
				//callSendAPI(2615959411751110, item.asset.fullName + " a atteint la cible customPrice Le " + today +" à " + i); 
			}
		}*/
	});
}

function getCurrentPrice(assets) {
		var test = 0;
		var ok = 0;
		console.log("taille " + assets.length);
		assets.forEach(function(item, index, array) {
			var requestOptions = {
				method: 'GET',
				uri: 'https://min-api.cryptocompare.com/data/price?fsym='+item.name+'&tsyms='+item.asset.devise
			};
			rp(requestOptions).then(res=>{
				for (let [key, value] of Object.entries(JSON.parse(res))) {
					item.cp = value;
				}

			}).catch((err) => {
				console.log(err.message);
			}).then(function() {
				test++;
				if(test == assets.length) {
					console.log(test);
					compareAsset(assets);
				}
				
			})
		})
}

    function callSendAPI(sender_psid, response) {
    console.log(response);
  // Construct the message body
  let request_body = {
    "recipient": {
      "id": sender_psid
    },
    "message": {
    	"text": response
    },
    "messaging_type": "MESSAGE_TAG",
    "tag": "NON_PROMOTIONAL_SUBSCRIPTION"
  }

  // Send the HTTP request to the Messenger Platform
  request({
    "uri": "https://graph.facebook.com/v2.6/me/messages",
    "qs": { "access_token": "EAAhYZCKh6ZBVQBAJYxetV4tSUtqOZBsUnzq5yV4kLvtPGvqln91C5dcOTQZCmyQYsNzbvaGrzxDZBGxkrmZCNo774e1DlZB1UefkZAskZAsOWmzAoZBFEbAhzoJPj0X6mVnEhlg3KIZB97LBQPiEJyGoUnoRtxyIJX9IT6pS2E7ILFZA1tgZCV1lLSuvI" },
    "method": "POST",
    "json": request_body
  }, (err, res, body) => {
    if (!err) {
      console.log('message sent!')
    } else {
      console.error("Unable to send message:" + err);
    }
  }); 
}
