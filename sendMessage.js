  const request = require("request")
  exports = module.exports =  {};
   exports.callSendAPI = function(sender_psid, response) {
    console.log(response);
  // Construct the message body
  let request_body = {
    "recipient": {
      "id": sender_psid
    },
    "message": {
    	"text": response
    },
    "messaging_type": "MESSAGE_TAG",
    "tag": "NON_PROMOTIONAL_SUBSCRIPTION"
  }

  // Send the HTTP request to the Messenger Platform
  request({
    "uri": "https://graph.facebook.com/v2.6/me/messages",
    "qs": { "access_token": "EAAhYZCKh6ZBVQBAIqgHwDeDeNVmXq2JvsJQGf4rZAZCksTkcWjsbSV5eUH2CqfMbrz2J3dEBZAziIlGJanzBnjaXwDFtjviiVhL9ajVKxoZB0xOljncjIhlXugbXtXaEBZC0rcVZAMTwssMeYq8KI4H9owXERJmaM1XkgA3nfLgOQ9DT5HdK6PZAbmKQc3gENKBMZD" },
    "method": "POST",
    "json": request_body
  }, (err, res, body) => {
    if (!err) {
      console.log('message sent!')
    } else {
      console.error("Unable to send message:" + err);
    }
  }); 
}